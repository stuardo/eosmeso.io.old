<!doctype html>
<html class="no-js" lang="zxx">
<head>
  <meta charset="utf-8">
  <meta name="author" content="John Doe">
  <meta name="description" content="">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Title -->
  <title>eosmeso / eosio Block Producer Candidate</title>
  <!-- Place favicon.ico in the root directory -->
  <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
  <link rel="shortcut icon" type="image/png" href="images/apple-touch-icon.png" />
  <!-- Plugin-CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <link rel="stylesheet" href="css/vendor/owl.carousel.min.css">
  <link rel="stylesheet" href="css/vendor/themify-icons.css">
  <link rel="stylesheet" href="css/vendor/magnific-popup.css">
  <link rel="stylesheet" href="css/vendor/animate.css">
  <!-- Main-Stylesheets -->
  <link rel="stylesheet" href="css/vendor/normalize.css">
  <link rel="stylesheet" href="css/vendor/responsive.css">
  <link rel="stylesheet" href="css/style.css">

  <script src="js/vendor/jquery-1.12.4.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


  <script src="js/vendor/modernizr-2.8.3.min.js"></script>

  <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target="#primary-menu" id="homepage">
<!--<div class="preloader">
  <div class="sk-folding-cube">
    <div class="sk-cube1 sk-cube"></div>
    <div class="sk-cube2 sk-cube"></div>
    <div class="sk-cube4 sk-cube"></div>
    <div class="sk-cube3 sk-cube"></div>
  </div>
</div>-->
<!--Mainmenu-area-->
<div class="mainmenu-area" data-spy="affix" data-offset-top="100">
  <div class="container">
    <!--Logo-->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a href="#" class="navbar-brand logo"> <img src="images/logo.png"/> </a> </div>
    <!--Logo/-->
    <nav class="collapse navbar-collapse" id="primary-menu">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="#home-page">Home</a></li>
        <li><a href="#about-page">Who We Are</a></li>
        <li><a href="#proposal-page">Our Proposal</a></li>
        <li><a href="#network-page">Network</a></li>
        <li style="display: none;"><a href="#client-page">Partners</a></li>
        <!-- <li><a href="#vote-page">Vote For Us</a></li> -->
        <li><a href="#contact-page">Contact Us</a></li>
      </ul>
    </nav>
  </div>
</div>
<!--Mainmenu-area/-->

<!--Header-area-->
<header class="header-area full-height relative v-center">
  <!--  <div class="absolute anlge-bg"></div>-->
  <div class="container">
    <div class="row v-center pri_main">
      <div class="col-xs-12 col-sm-12 col-lg-2 text-center"> </div>
      <div class="col-xs-12 col-sm-12 col-lg-8 text-center jumbotron">
        <img src="images/logo-big.png" />
        <p>Resilient, fast and flexible infrastructure, rapid growth capacity, crosspolination from diverse partnerships, demonstrated community involvement.  </p>
        <p>eosmeso - set in stone.</p>
      </div>
      <div class="col-xs-12 col-sm-12 col-lg-2 text-center"> </div>
    </div>
  </div>
</header>
<!--Header-area/-->

<section class="dark-bg" id="pillars">
    <div class="container">
      <div class="row v-center">
        <div class="col-xs-12">
          <h2 class="mm_title text-center">OUR PILLARS</h2>
          <h4 class="col-xs-4 text-center"> 1. STRENGTHEN THE EOS NETWORK</h4>
          <h4 class="col-xs-4 text-center"> 2. FOSTER ADOPTION AND USE</h4>
          <h4 class="col-xs-4 text-center"> 3. ENGAGE VOTERS ETHICALLY</h4>
        </div>
      </div>
    </div>
  </section>


    <section class="testimonial-area section-padding gray-bg-my" id="proposal-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4"><img src="images/vision.png" alt=""></div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <h2 class="mm_tite">VISION - Why Mesoamerica </h2>
            <p>Our vision is to be one of the 21 main block producers for the main EOSIO blockchain and be responsible stewards of the transition to a world of transparency, user owned data and abundance.  Mesoamerica is a keystone in the world internet topography, EOSMESO has the knowledge, relationships and financing to leverage this fact in service of the eos holders.</p>
            <p>Mesoamerica is the cultural and geographic region joining the North and South of the Americas.  Our region has a very diverse population and bioregions, rich in subcultures, at short distance from each other.  Because of this diversity, we believe to be an ideal testing ground for developing world blockchain applications that have analogous characteristics to several other parts of the developing world. EOSMESO is headquartered in Guatemala, at the heart of MesoAmerica, the navel of the earth.  We want Guatemala to become ground zero for EOSIO based solutions to  developing world problems relating to water, land use, denomination of origin and supply chain transparency, institutional accountability, insurance and reinsurance, micro investment and other use cases in need of reliable, affordable and transparent transactions.  With ceiba.io we have already made good strides in this direction. If elected as Block Producers, this part of our mission will be strongly bolstered.  Through our diverse network of investors and partners we can foster this vision, alongside global leaders in the field.</p>
          </div>
        </div>
      </div>
    </section>


<section class="angle-bg white-bg section-padding" id="about-page">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel">
          <div class="carousel-inner" role="listbox" role="listbox">
            <h2>WHO WE ARE? </h2>
            <?php require_once '_who.php' ?>
          </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Who We are-->


<div class="testimonial-area1">
  <section class="section-padding gray-bg-my" id="proposal-page">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4"><img src="images/our-proposals.png" alt=""></div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
        <h2 class="mm_tite">OUR PROPOSAL</h2>
        <div class="testimonials">
          <div class="testimonial">
            <h3>(Strengthen the EOS blockchain) To strengthen the main EOS blockchain by adding to its antifragility</h3>
            <p>How? With fast, reliable, geographically and jurisdictionally flexible, redundant and secure computational infrastructure, adhering to the principles of life, liberty and property. </p>
            <p><b>Contract features: </b></p>
            <ul>
              <li> Fast and Reliable: Tier 4 ISO certified Datacenter structure with flexibility to run  64MB-512RAM servers according to network needs, run inside two separate datacenters within the infrastructure handling 70% of the internet traffic of Guatemala. </li>
              <li> (Certificaciones ISO del datacenter, imagen de la redundancia de “salidas” a internet que tiene TIGO) </li>
              <li> Geographically and jurisdictionally flexible:  Option to run a mirror service in Colombia, Costa Rica, Panama or Paraguay. Option to backup with alternative operator.  Option to finance and locate stronger machines when needed.  All within a few hours reach. </li>
              <li> Secure layout featuring front facing and back facing servers.  (Imagenes de Layout) </li>
            </ul>
          </div>
          <div class="testimonial">
            <h3>To have the ability to rapidly grow our capacities along with the main EOS blockchain</h3>
            <ul>
              <li>Very strong investor pool ready to meet our growth needs  Click “Jump to partners”, </li>
              <li>Capital calls at management´s discretion</li>
              <li> Credit lines established.  (Ver imagenes de contratos:  inversionista y l. credito) </li>
              <li>Always in anticipation of the network needs,  (click para explicar roadmap de escalabilidad)</li>
            </ul>
          </div>
          <div class="testimonial">
            <h3>To foster the adoption of the main EOS blockchain by leveraging our networks</h3>
            <ul>
              <li>Our partners have strong local and global networks in diverse fields like app development, venture capital, ticketing, international commerce, commodities export, trade finance, university, real estate development, microfinance, IT infrastructure, energy, telecomm, music and film.</li>
              <li>Some of our partners and both our managers are founding members of ceiba.io, a for impact nonprofit association educating public and private groups through free of charge workshops and publications since 2016.    EOSMESO commits to donating 1% of our block reward revenues directly to ceiba.io for it to continue its labor of educating the business and tech community in MESOAMERICA about the improvements to society that can be achieved through the use of blockchain technology.  (click “learn more” para explicar ir a parte que explica ceiba.io)</li>
            </ul>
          </div>
          <div class="testimonial">
            <h3>To be transparent in the use of resources, our relationships to providers, investors, partners and the network at large. </h3>
            <ul>
              <li> The way we see it, the EOS network will power a much more trasnparent and auditable world.</li>
              <li>As responsible stewards of the network in our region, we believe we must set an example of what is to be expected from a business of the future.  Therefore, we will to publish all our company data, allowing for the privacy of individual investors only, if they so chose. </li>
              <li> Our costs, strategy, performance, income, investments and donations shall be public. </li>
              <li> Our capitalization model can be seen here. (Jose falta escribir aquí)</li>
            </ul>
          </div>
          <div class="testimonial">
            <h3>To engage the voting eos tokenholders in an ethical fashion</h3>
            <ul>
              <li> We invite all eos holders to vote for us because of the benefits we provide to the EOS blockchain network as a whole, and not because of “perks” or rewards directly attributable to a particular vote.  We aim to inspire and educate the eos holders about the virtues of keeping voting separate from investing as a way to safeguard the network. </li>
              <li> Our partners hold units in the business which entitle them to receive most of the eos we earn as Block Producers.  These units require dollar inputs, to prevent them from expiring, when management sees the need for capital calls. We have assembled a broad investor pool that guarantees proper capitalization, as long as management succeeds and we gain the trust of the eos voters.   We visualize these units as DAC tokens in the future, trading in the open market and believe this model to properly allign the incentives of management, investors and eos tokenholders. </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>

<!-- tesimonials -->

<section class="gray-bg section-padding" id="network-page">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
        <div class="page-title">
          <h2 class="mm_tite">EOS NETWORK LATIN AMERICA</h2>
          <p>Our network Infrastructure solution is based in Central America connecting to the world through the NAP of the Américas, by five redundant, high performance fiber optic network connections.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="box">
          <div class="box-icon"> <img src="images/Netowk-icon1.png" id="myBtn" alt=""></div>
          <h3>ISO 22301:2012</h3>
          <p>Business continuity management system for the process of operation and maintenance, to ensure availability of infrastructure services and software services in the cloud.</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="box">
          <div class="box-icon"> <img src="images/Netowk-icon3.png" id="myBtn1" alt=""> </div>
          <h3>Data Center</h3>
          <p>Our main block producers, full nodes and backup/standby full nodes will be located at the same data center.
            Production infrastructure will start with a cloud solution hosted in Guatemala city with Tigo Business, a Millicom Partner.
            </p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="box">
          <div class="box-icon"> <img src="images/Netowk-icon2.png" id="myBtn2" alt=""> </div>
          <h3>ISO/IEC 27001:2013</h3>
          <p>Design of Solutions, service delivery, operation and maintenance of services regarding software, platform and infrastructure in the Cloud.</p>
        </div>
      </div>
      <!-- First Lightbox  -->     <div id="myModal" class="modal">


        <div class="modal-content">
          <div class="modal-header"> <span class="close">&times;</span> <img src="images/cert_22301.png"/> </div>
        </div>
      </div>

    <!-- Second Lightbox  -->
      <div id="myModal1" class="modal1">
        <div class="modal-content">
          <div class="modal-header"> <span class="close1">&times;</span> <img src="images/network.png" /> </div>
        </div>
      </div>

       <!-- Third Lightbox  -->
      <div id="myModal2" class="modal2">
        <div class="modal-content">
          <div class="modal-header"> <span class="close2">&times;</span> <img src="images/cert_27001.png"/> </div>
        </div>
      </div>

      <!-- Lightbox end-->
    </div>
  </div>
</section>

<section class="section-padding client-area" id="client-page" style="display: none;">
  <div class="container">
    <div class="row text-center">
      <div class="col-xs-12" style="margin-bottom:50px;">
        <h2 class="mm_tite">OUR PARTNERS</h2>
        <p>Proin efficitur bibendum diam, sit amet dapibus leo maximus bibendum. Vestibulum ex lacus, rutrum id mi imperdiet, semper cursus magna. Donec ut laoreet velit, eget elementum ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque sit amet commodo metus, nec porttitor nulla. Ut sit amet nisl tortor. </p>
      </div>
    </div>
    <div class="row text-center">
      <div class="col-xs-12">
        <div class="clients">
          <div class="item"> <img src="images/themeforest.gif" alt=""> </div>
          <div class="item"> <img src="images/coadcanyon.gif" alt=""> </div>
          <div class="item"> <img src="images/graphicriver.gif" alt=""> </div>
          <div class="item"> <img src="images/docean.gif" alt=""> </div>
          <div class="item"> <img src="images/audiojungle.gif" alt=""> </div>
          <div class="item"> <img src="images/actividen.gif" alt=""> </div>
          <div class="item"> <img src="images/photodone.gif" alt=""> </div>
          <div class="item"> <img src="images/videgub.gif" alt=""> </div>
          <div class="item"> <img src="images/themeforest.gif" alt=""> </div>
          <div class="item"> <img src="images/coadcanyon.gif" alt=""> </div>
          <div class="item"> <img src="images/graphicriver.gif" alt=""> </div>
          <div class="item"> <img src="images/docean.gif" alt=""> </div>
          <div class="item"> <img src="images/audiojungle.gif" alt=""> </div>
          <div class="item"> <img src="images/actividen.gif" alt=""> </div>
          <div class="item"> <img src="images/photodone.gif" alt=""> </div>
          <div class="item"> <img src="images/videgub.gif" alt=""> </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="dark-bg" id="vote-page" style="display: none;">
  <div class="container">
    <div class="row v-center text-center">
      <div class="caption-button col-xs-12 col-sm-12 text-center" data-animation="animated fadeInUp">
        <a href="#" class="button btn-lg" id="mailChimp">Vote For Us</a>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: true"></script>
  <script type="text/javascript">
    $(function() {
      $('#mailChimp').click(function() {
        document.cookie = "MCPopupClosed=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
        document.cookie = "MCPopupSubscribed=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
        require(["mojo/signup-forms/Loader"], function(L) {
          L.start({"baseUrl":"mc.us18.list-manage.com","uuid":"0f231734baa64b78cfbf23a59","lid":"cc0112c5f5"})
        });
        console.log('salgo');
        return false;
      });
    });
  </script>
</section>


<footer class="footer-area sky-bg" id="contact-page">
  <div class="absolute footer-bg"></div>
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
          <div class="page-title">
            <h2>Questions + Comments</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-4">
          <address class="side-icon-boxes">
          <div class="side-icon-box">
            <div class="side-icon"> <img src="images/location-arrow.png" alt=""> </div>
            <p><strong>Address: </strong>
              Box 564, Your Address Here <br />
              Guatemala City, Guatemala</p>
          </div>
          <div class="side-icon-box">
            <div class="side-icon"> <img src="images/phone-arrow.png" alt=""> </div>
            <p><strong>Telephone: </strong> <a href="callto:2028880782">+1 202 888 0782</a> <br />
              <a href="callto:2028880782">+1 202 888 0782</a> </p>
          </div>
          <div class="side-icon-box">
            <div class="side-icon"> <img src="images/mail-arrow.png" alt=""> </div>
            <p>
              <strong>E-mail: </strong>
              <a href="mailto:info@eosmeso.io">info@eosmeso.com</a> <br />
          </div>
          </address>
        </div>
        <div class="col-xs-12 col-md-8">
          <form id="contact-form" method="post" class="contact-form">
            <div class="form-double">
              <input type="text" id="form-name" name="form-name" placeholder="Your name" class="form-control" required>
              <input type="email" id="form-email" name="form-email" class="form-control" placeholder="E-mail address" required>
            </div>
            <input type="text" id="form-subject" name="form-subject" class="form-control" placeholder="Message topic">
            <textarea id="form-message" name="form-message" rows="5" class="form-control" placeholder="Your message" required="required">
            </textarea>
            <button type="sibmit" class="button">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-middle">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 pull-right">
          <ul class="social-menu text-right x-left">
            <li><a target="_blank" href="https://www.facebook.com/Eosmeso-244654882940957/"><i class="ti-facebook"></i></a></li>
            <li><a target="_blank" href="https://twitter.com/eosmeso"><i class="ti-twitter"></i></a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-6">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id corrupti architecto consequuntur, laborum quaerat sed nemo temporibus unde, beatae vel.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center">
          <p>&copy;Copyright © 2018 eosmeso.io. </p>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--Vendor-JS-->
<!--Plugin-JS-->
<script src="js/owl.carousel.min.js"></script>
<script src="js/contact-form.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/scrollUp.min.js"></script>
<script src="js/magnific-popup.min.js"></script>
<script src="js/wow.min.js"></script>
<!--Main-active-JS-->
<script src="js/main.js"></script>
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}



// Get the modal
var modal1 = document.getElementById('myModal1');

// Get the button that opens the modal
var btn1 = document.getElementById("myBtn1");

// Get the <span> element that closes the modal
var span1 = document.getElementsByClassName("close1")[0];

// When the user clicks the button, open the modal
btn1.onclick = function() {
    modal1.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span1.onclick = function() {
    modal1.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal1) {
        modal1.style.display = "none";
    }
}





// Get the modal
var modal2 = document.getElementById('myModal2');

// Get the button that opens the modal
var btn2 = document.getElementById("myBtn2");

// Get the <span> element that closes the modal
var span2 = document.getElementsByClassName("close2")[0];

// When the user clicks the button, open the modal
btn2.onclick = function() {
    modal2.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span2.onclick = function() {
    modal2.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal2) {
        modal2.style.display = "none";
    }
}



</script>
</body>
</html>
