<?
$founders = [
    [
        'name' => 'Jose “Fire is Born” Toriello - Siyaj K’ak',
        'role' => 'Founding Team / Head of Strategy and Relations',
        'img'  => 'Jose Toriello.jpg',
        'desc' => 'Jose has inspired many in Guatemala and the world to discover bitcoin and blockchain since 2014.  His his passion for ethics, transparency and a “world of we” has taken him to dedicate full time to  since late 2016.  Since the publication of the eos.io whitepaper, he has been sharing his belief that “this is it” with any one who will listen.  He is founding member of EOSMESO, ceiba.io, Bullbro Inc, weloveguatemala.com, and volunteer, board member and donor to Asociación de Amigos del Lago de Atitlán, a for impact organization working to install a water utility, lake saving, energy generating, wastewater management infrastructure in Mesoamerica\'s most iconic lake, Lake Atitlán',
        'link' => 'https://www.linkedin.com/in/jose-antonio-toriello-herrer%C3%ADas-358b98135/',
    ], [
        'name' => 'Luis “Quetzal Guacamaya” Gramajo',
        'role' => 'Founding Team / Head of Infrastructure and Operations',
        'img'  => 'Luis Gramajo.jpg',
        'desc' => 'Luis is known in Guatemala´s “Silicon Alley” blockchain circles as “The Blockchain Prof”, for his ample knowledge of the different protocols out there .  His 20+ year career in IT infrastructure management has seen him manage server farms for several local banks, utility companies and others.
                His interest in the open source software eventually led him to mine bitcoin on a USB stick.
                His nickname “El Prof.” became uncanny when Universidad Francisco Marroquín approached him to help design and implement the blockchain curricula, a collaboration of the economics, computer science and law faculties.',
        'link' => 'https://www.linkedin.com/in/jose-antonio-toriello-herrer%C3%ADas-358b98135/',
    ], [
        'name' => 'Stuardo “Precious Peccary” Rodriguez - K\'an Chitam',
        'role' => 'Partners and advisors / Tech and Infrastructure',
        'img'  => 'Stuardo Rodríguez.jpg',
        'desc' => 'Stuardo is an experienced backend software developer who has been a devoted member of the EOSIO developer community.  EOS was the first protocol to pass his skeptical mind´s test of real world use.  He is working with Prof Gramajo on making sure our 24/7 monitoring team is up to speed on IT management.  He will collaborate with EOSMESO to develop tools for EOSIO and advise in our direction.
                    He is the creator of transparente.gt, and has succeeded, after persisting for 6 years, in getting the Guatemalan government to share data in useful forms for citizens to be able to audit their government.',
        'link' => 'https://www.linkedin.com/in/stuardo',
    ]
];

$who = [
    [
        'name' => 'André “The Talent” Gamez',
        'role' => 'Partners and advisors / Influencers and Media Partners',
        'img'  => 'Andre Gamez.jpg',
        'desc' => 'André Gamez is a guitar prodigy and head of the INTERGALACTIC SOUNDSYSTEM music studio.  He plays lead guitar for “Filoxera”, the most exciting rock band in the region, and the first to accept criptocurrency as payment for their shows. . Graduate of Berklee School of Music,  his connections to the music industry along with his desire to have a more transparent and artists friendly industry are the reasons for his having been invited to be a partner in eosmeso.',
    ], [
        'name' => 'Arturo “The Star” Castro',
        'role' => 'Partners and advisors / Influencers and Media Partners',
        'img'  => 'Arturo Castro.jpg',
        'desc' => 'Actor, producer, director and a very funny guy, Arturo has starred in the Netflix series “Narcos” and been cast member for several Comedy Central programs like “Broad City”, “Alterlatino”, and his own show now in production “”.  His popularity across people in North and Mesoamerica and his involvement in EOSMESO is a very unique asset.  He was named “Distinguished Citizen” by ___ last year, and has led several charitable campaigns since before his entertainment career took off.',
        'link' => 'https://m.imdb.com/name/nm3594330/',
    ], [
        'name' => 'Carlos Enrique “The Authority”  Newbill',
        'role' => 'Partners and advisors / Finance and Law',
        'img'  => 'Carlos Enrique Neuvi.jpg',
        'desc' => 'Investment professional with over 18 years experience in multi-asset investing.  Member of the Finance Committee at Grupo Progreso since 2004.  Also, Risk Committee member and board director at Financiera Progreso.
                Adjunct professor of Entrepreneurial Finance, Equity Analysis and Capital Markets at UFM since 1999.
                Interested in enhanced encryption for the improvement of global business and individual liberty. Plays a solid rock and roll drums.',
        'link' => 'https://en.ufm.edu/catedraticos/carlos-newbill/',
    ], [
        'name' => 'Darío “The Heart” Martinez',
        'role' => 'Partners and advisors / Finance and Law',
        'img'  => 'Darío Martínez.jpg',
        'desc' => 'Darío has taught postgraduate law at Universidad de San Carlos in the Constitutional Law, Tributary Law and Human Rights fields. After his profound knowledge of the Guatemalan Republics Institutional fallencies made him depressed, he found hope in the promise of blockchain technology.  He has been consulting for the Guatemalan Banking Association and Registro de mercados de Valores y Mercancías (analogous to the SEC), working for the demystification of cryptocurrency for the past 3 years.  When he attended one of the first ceiba meetups, he was overjoyed to find that he “was not alone” in his quest to improve our social operating systems.',
        'link' => '',
    ], [
        'name' => 'Eduardo “The Mind” Cuevas',
        'role' => 'Partners and advisors / Finance and Law',
        'img'  => 'Eduardo Cuevas.jpg',
        'desc' => 'Eduardo teaches the “Private Equity” and "Corporate Risk Management" classes at Universidad Francisco Marroquín and has served in different management positions for several international and local financial institutions for the last 18 years.  He has originated and supervised corporate debt and quasi-equity portfolios in the Central America region (holdings in each portfolio consistently exceeded $300 million), and was part of transaction teams leading global bond issuances, project finanace, and M&A transactions.',
        'link' => 'https://www.linkedin.com/in/eduardo-cuevas-8590642/',
    ], [
        'name' => 'Fernando “Makoto Me” Pontaza',
        'role' => 'Partners and advisors / Business Development',
        'img'  => 'Fernando Pontaza.jpg',
        'desc' => 'Fernando is GP at Invariantes Fund, Guatemala\'s first early-stage venture capital firm, connecting companies, capital and talent across the Americas, investing in software companies.   He is EOSMESO\'s liason to the VC world in SF, LATAM and beyond.  He is also active in the local jiu jitsu and cross fit communities.',
        'link' => 'https://www.linkedin.com/in/fpontaza/',
    ], [
        'name' => 'Gloria “Ixkalomté” Alvarez',
        'role' => 'Partners and advisors / Influencers and Media Partners',
        'img'  => 'Gloria Alvarez.jpg',
        'desc' => 'Queen of libertarian radio in LATAM, Gloria has a passion for logic, free speech and open dialogue.  She exposed the lies of corrupt candidates for Guatemala´s presidential elections in 2014 and led many pacific protests during the tumultuos resignation of the then Vice-President and Presidents of the government of the Republic.   EOSMESO is sponsoring her “Freedom Friday shows”, with reach to over 70,000 weekly listeners.',
        'link' => 'https://twitter.com/GloriaAlvarez85',
    ], [
        'name' => 'Jose “Pepe Satoshi” Guillén',
        'role' => 'Partners and advisors / Influencers and Media Partners',
        'img'  => 'Jose Guillen.jpg',
        'desc' => 'Founding member of ACUCRIP and member of the “ceiba.io 21”, José has travelled throughout Central America to spread the adoption of sound money.  He will help to strengthen EOSMESO outside Guatemala to all the regions of MESOAMERICA. He, Jose Toriello, and Eduardo Cuevas share the dubious honor of being summoned by the Central Bank of Guatemala to explain basic concepts about bitcoin and blockchain.
                    He is passionate about decentralized economics and is part of a land reclamation biodynamic agriculture project that has turned what was once a fallow "potrero" into a lush, productive and organic source of nutrition in the eastern lands of Guatemala.',
        'link' => '',
    ], [
        'name' => 'Josué “The Guru” García',
        'role' => 'Partners and advisors / Influencers and Media Partners',
        'img'  => 'Josué Garcia.jpg',
        'desc' => 'Is a rockstar bass player and microfinance guru. With Bohemia Suburbana, the biggest rock band out of Central America, he has toured throughout central america, and several cities in the USA like New York, Houston, Maryland, LA, San Diego, Connecticut, DC playing for migrant and local fans.He has shared the stage with Red Hot Chilli Peppers, Incubus, Matisyahu, Bomba Stereo and Maná, among others ….
                    His 12 year career in microfinance has seen him innovate in the field, now in charge of overseeing 8$M in microloans in the western highlands of Guatemala for a firm with over 20,000 clients. He is spearheading the switch from cash to electronic money.',
        'link' => 'https://www.linkedin.com/in/josue-garcia-19010015a/',
    ], [
        'name' => 'Mikele “Two birds” Cessia - Wak Chan kawil',
        'role' => 'Partners and advisors / Tech and Infrastructure',
        'img'  => 'Mikele Ceschia.jpg',
        'desc' => 'Our liason with our first cloud infrastructure UPTIME provider.  Mikele has an 18 year career in Business Development for the leading telecomm provider in the region. His role is to ensure EOSMESO needs will be met quickly and effectively.  He is passionate about using technology to “leapfrog” the sustainable development of our region.',
        'link' => 'https://www.linkedin.com/in/mikele-ceschia-247183123/',
    ]
];
shuffle($who);
$active = true;
$who    = $founders + $who;
?>
<? foreach ($who as $person) :?>
    <div class="item row <?php if ($active) {echo 'active'; $active = false; } ?>">
        <div class="v-center">
            <div class="col-xs-12 col-md-6 col-lg-9">
                <div class="caption-title" data-animation="animated fadeInUp">
                    <h3><?=$person['name']?></h3>
                    <h4><?=$person['role']?></h4>

                </div>
                <div class="caption-desc" data-animation="animated fadeInUp">
                    <p><?=nl2br($person['desc'])?></p>
                </div>
                <? if ( !empty($person['link']) ) :?>
                    <div class="caption-button" data-animation="animated fadeInUp">
                        <a class="button" target="_blank" href="<?=$person['link']?>" >More about <?=$person['name']?></a>
                    </div>
                <? endif ?>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-3">
                <div class="one" data-animation="animated fadeInRight">
                    <img src="images/who/<?=$person['img']?>" class="img-circle" alt="<?=$person['name']?>">
                </div>
            </div>
        </div>
    </div>
<? endforeach ?>

<div class="row" style="margin-top: 1em;">
    <div class="col-xs-1">
        <a class="left _carousel-control" href="#caption_slide" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
    </div>
    <div class="col-xs-10">
        <? $active = true; ?>
        <ol class="carousel-indicators">
        <? foreach ($who as $i => $person) :?>
            <li data-target="#caption_slide" data-slide-to="<?=$i?>" class="<?php if ($active) {echo 'active'; $active = false; } ?>"></li>
            <? endforeach ?>
        </ol>

    </div>
    <div class="col-xs-1">
        <a class="right _carousel-control" href="#caption_slide" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
